# Importing Libraries
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

# Importing Datasets
importset = pd.read_csv("2018-2010_import.csv")
exportset = pd.read_csv("2018-2010_export.csv")

# Most Countries Exported To Per Year
plt.figure(figsize=(50,50))
j = 0
for i in ['2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017', '2018']:
	j+=1
	plt.subplot(3,3,j)
	plt.title("Most Countries Exported in " + i)
	sns.countplot(x = exportset[exportset.year == int(i)].country, order = exportset[exportset.year == int(i)].country.value_counts().index[0:10])
	
# Most Countries Imported To Per Year
plt.figure(figsize=(50,50))
j = 0
for i in ['2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017', '2018']:
	j+=1
	plt.subplot(3,3,j)
	plt.title("Most Countries Imported in " + i)
	sns.countplot(x = importset[importset.year == int(i)].country, order = importset[importset.year == int(i)].country.value_counts().index[0:10])
	
# Country With The Highest Values Of Export
plt.figure(figsize = (50,50))
j = 0
for i in ['2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017', '2018']:
	j+=1
	plt.subplot(3,3,j)
	x = exportset[exportset.year == int(i)].groupby('country')['value'].agg('sum').sort_values(ascending=False)[:10].index
	y = exportset[exportset.year == int(i)].groupby('country')['value'].agg('sum').sort_values(ascending=False)[:10]
	sns.barplot(x=x,y=y)
	plt.title("Top 10 Most Valuable Countries Exported in " + i)
	plt.ylabel("$Million")
	
# Country With The Highest Values Of Import
plt.figure(figsize = (50,50))
j = 0
for i in ['2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017', '2018']:
	j+=1
	plt.subplot(3,3,j)
	x = importset[importset.year== int(i)].groupby('country')['value'].agg('sum').sort_values(ascending = False)[:10].index
	y = importset[importset.year== int(i)].groupby('country')['value'].agg('sum').sort_values(ascending = False)[:10]
	sns.barplot(x=x,y=y)
	plt.title("Top 10 most Valuable Countries Imported in " + i)
	plt.ylabel("$Million")